# Leap year checker

year = int(input("Please provide a year: \n"))

# int_checker = isinstance(year, int) # year already has int(), so inputting a string will cause a ValueError

# Not negative number
if year > 0 :

	# Check divisibility of year by 4
	if year % 4 == 0 :
		
		# Not divisible by 100
		if year % 100 != 0 :
			print(f"{year} is a leap year.")

		# Divisible by 100
		else :

			# Divisible by 100 and 400
			if year % 400 == 0 :
				print(f"{year} is a leap year.")

			# Not divisible by 100 and 400	
			else :
				print(f"{year} is not a leap year.")

	# Not divisible by 4
	else :
		print(f"{year} is not a leap year.")

else :
	print(f"{year} is an invalid year. Please try again.")

# Asterisk Grid

row = int(input("Enter number of rows: \n"))
col = int(input("Enter number of columns: \n"))

row_x = 1
col_x = 1

for row_x in range(row) :
	for col_x in range(col) :
		print("*", end = "")
	print()