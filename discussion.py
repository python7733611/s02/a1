# Python also allows users to input data. With this, users can give input to the program.

# [Section] Input
# username = input("Please enter your name: \n") # \n represents a new line.
# print(f"Hello, {username}! Welcome to the Python Short Course.")

# num1 = int(input("Enter first number: \n"))
# num2 = int(input("Enter second number: \n"))
# print(f"The sum of num1 and num2 is {num1+num2}.")

# Using user inputs, users can give inputs to control the application using control structures.
# Control structures can be divided into selection and repetition structures.

# Selection control structures allow the program to decide and run specific codes depending on the choice it took. (i.e. conditions in JS)
# Repetition control structures allow the program to iterate certain blocks of code given a starting condition and terminating condition. (i.e. loops for code in JS)

# [Section] If-Else statements (elif rather than else if)
# These are used to choose between two or more code blocks depending on given conditions.

test_num = 75

if test_num >= 60 :
	print("Test passed!")
else :
	print("Test failed!")

# Note that in Python, curly braces ({}) are not needed to distinguish the code blocks inside the if or else block. Hence, indentations are important as they are used to distinguish parts of code as needed.

# [Section] If-else chains can also be used to have more than 2 choices for the program.

# test_num2 = int(input("Please enter the second number: \n"))

# if test_num2 > 0 :
# 	print("The number is positive.")
# 	print(f"The number you provided was {test_num2}.")
# elif test_num2 == 0 :
# 	print("The number is equal to zero.")
# else :
# 	print("The number is negative.")

# [Mini-Activity]

# num = int(input("Please enter a number:\n"))

# if num % 3 == 0 and num % 5 == 0 :
# 	print(f"{num} is divisible by 3 and 5.")
# elif num % 5 == 0 :
# 	print(f"{num} is divisible by 5.")
# elif num % 3 == 0 :
# 	print(f"{num} is divisible by both 3.")
# else:
# 	print(f"{num} is divisible by neither 3 nor 5.")

# [Section] Loops
# Python, like JS, has loops that can allow a program to repeat blocks of code.

# While-loops are used to execute a set of statements as long as the given condition is true.

i = 0

while i <= 5 :
	print(f"The current value of i is {i}.")
	i += 1

i = 0

while i <= 5 :
	i += 1
	print(f"The current value of i is {i}.")
# Note that this ends at 6 unlike the earlier while-loop example because the incrementation occurs before the print command.

# [Section] for-loops are used for iterating over a sequence. (similar to JS's foreach)
# Syntax:
	# for identifier in list_name :

fruits = ["apple", "banana", "cherry"] # JS : array :: Python : list

for indiv_fruit in fruits :
	print(indiv_fruit)

# print(len(fruits)) # returns 3

# [Section] range() method; used with integrets
# To use the for-loop to iterate through values, the range method can be used
# Syntax:
	# range(starting_value, limit, incrementation_value)
	# If a single value is placed inside the parentheses (), the code will interpret it as the limit or stopper.

# for x in range(6):
# 	print(f"The current value of x is {x}.")

# By default, the iteration starts at 0 in range(). The value inside range() represents the number of times the code will iterate.

# for x in range(6, 10):
# 	print(f"The current value of x is {x}.")

# 6 becomes the starting value for 6, and the code will iterate just before x's value is 10 (stops with x = 9).

# for x in range(6, 20, 2):
# 	print(f"The current value of x is {x}.")

# 2 represents the incrementation value of range(). If it is not specified, the default incrementation is +1.

# [Section] Break Statement
# Break is used to stop the code from looping again.

# j = 1
# while j < 6 :
# 	print(j)

# 	if j == 3:
# 		break

# 	j += 1
# Notice the indentations for each nested condition.

# [Section] Continue Statement
# Continute tells the code to return to the beginning of the while loop and continue to the next iteration.

k = 1
while k < 6 :

	k += 1

	if k == 3 :
		continue

	print(k)

# The placement of the incrementation is important for loops.
# If k += 1 is placed after the k == 3 condition, the code will result in a kind of infinite loop because k's value is still less than 6 AND is equal to 3. It will try to continue to the next iteration, but because k would still be equal to 3, nothing will happen.